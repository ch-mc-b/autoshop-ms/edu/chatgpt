Version 1 - erste Version
=========================

[[_TOC_]]

Erste Version, einfaches HTML und REST API

Version 1.0.0 (erste Version)
-------------

**ChatGPT Anweisungen**:

    Basierend auf den Microservices Beispiel von Eberhard Wolff schreibe mit 3 Python Scripte welche jeweils eine Webseite mit
    - Kundendaten (Customer)
    - Produktdaten (Catalog)
    - Bestellungen (Order)
    und ein Einsteigerscript in Python mit einem Menü für den Wechsel auf Customer, Catalog und Order enthält.
    Die Script sollen ein REST-API /<service>/api und die Daten im HTML-Format als /<service/ zur Verfügung stellen.
    Die Produkte sollen dem eines Auto Hauses mit Autos und Auto Zubehör Artikeln entsprechen.

    Als TCP/IP Port soll generell 8080 und die Services sollen gegen 0.0.0.0 geöffnet sein.
    Das Einsteigerscript soll das Reverse Proxy Design Pattern verwenden.

Anschliessend bereiten wir die Services als Container auf.

    Erstelle für die Services ein Dockerfile und ein Docker-Compose Datei um es starten zu können.

Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v1.0.0?ref_type=heads) ersichtlich.

**Tips und Tricks**

Man kann ChatGPT auch bitten gewisse Teile des Codes neu zu generieren. Dazu kombiniert man die Frage mit einem Codestück, z.B.

    Erweitere den Code so, dass es die grossen Automarken und Modelle beinhaltet.

    products = [
        {"id": 1, "name": "Car Model X"},
        {"id": 2, "name": "Car Model Y"},
        {"id": 3, "name": "Wheel Accessory"},
    ]
            