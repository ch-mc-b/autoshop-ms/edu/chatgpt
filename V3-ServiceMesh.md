Service Mesh
============

[[_TOC_]]

Version 3.0.0 (Metrics Data)
-------------

Es sollen mittels dem URL `/metrics` [Prometheus]() kompatible Metrics Daten ausgegeben werden.

**ChatGPT Anweisungen**:

    Erweitere den Code von https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/raw/main/order/order_service.py?ref_type=heads 
    um Metrics Informationen für Prometheus

Diese Anweisung ist für die restlichen Services (Catalog, Customer, Webshop) zu wiederholen.    

Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v3.0.0?ref_type=heads) ersichtlich.     

Version 3.1.0 (Back Office)
-------------

Implementierung der Back Office Services 
* `invoicing` - Rechnungstellung
* `shipment` - Versand

Erweiterung von `reverse_proxy` um deren Anwahl.

**ChatGPT Anweisungen**:

Startet die Shop Applikation und wählt [http://localhost:8080/order/order/api](http://localhost:8080/order/order/api) an. Das liefert die Bestellungen im JSON-Format.

Mit den Bestellungen als Input erstellt `invoicing`.

    Erstelle mit anhand der nachfolgenden JSON Daten einen Service invoicing..
    Der Service sollte die Daten auf Port 8080 für 0.0.0.0 ausgeben.
    Die Daten sind dabei als HTML-Tabelle auszugeben.
    Für eine schönere Ausgabe verwende die Bootstrap Library.
    Füge ein Total pro Kunde (customer) und ein Gesamttotal ein.
    
    Hier sind die Daten:
    <Copy der JSON Daten>

Dann müssen wir noch `reverse_proxy` um `invoicing` erweitern 
    
    Erweitere folgenden Code damit auch invoicing angesprochen werden kann    
    <Copy Sourcecode reverse_proxy.py>

Und abgeleitet von `invoicing` noch `shipment` erstellen

    Erstellen Abgeleitet vom invoicing Service einen Service shipment.
    Statt den Betrag zu Summieren, Summiere das Feld Menge (quantity)

**Tips und Tricks**

Für `Dockerfile` und `requirements.txt` verzichtet man am Besten für einen Generierung und kopiert die einfach von `reverse_proxy` und passt die `CMD` Zeile im `Dockerfile` manuell an.

Ergänzt man die Frage um `Erstelle in Ableitung von order_service.py` und kopiert den Code rein, ist die Code Generation konsistenter mit dem bestehenden Code.  
    
Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v3.1.0?ref_type=heads) und 
[hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/backoffice/-/tree/v3.1.0?ref_type=heads) ersichtlich. 

Version 3.2.0 (Management Dashboard)
-------------

Implementierung des Management Dashboards mit Umsatzzahlen.

**ChatGPT Anweisungen**:

    Erstelle mir einen sales Service in Python welche die Order Daten von order_service.py via REST ausliest.
    Erstellt mir eine HTML Tabelle aller Produkte mit deren Total pro Produkt und darunter einen HTML Tabelle 
    mit allen Kunden und einem Total pro Kunde.
    
Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v3.2.0?ref_type=heads) und 
[hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/management/-/tree/v3.2.0?ref_type=heads) ersichtlich.     
    
  