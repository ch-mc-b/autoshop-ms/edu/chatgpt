Version 2 - REST API, verbesserte Anzeige
=========================================

[[_TOC_]]

Version 2.0.0 (verbesserte Anzeige)
-------------

Verbesserte Anzeige (Bootstrap)

**ChatGPT Anweisungen**:

    Verbessere die HTML Ausgabe um eine schöner formatierte Anzeige.

**Tips und Tricks**    

Falls das Resultat noch nicht zufriedenstellt, einfacher präziser Fragen 

    Verbessere die HTML Ausgabe mit bootstrap

Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v2.0.0?ref_type=heads) ersichtlich. 

Version 2.0.1 (REST-API)
-------------

Verbessertes REST-API, einzelne Abfrage eines Datensatzes

**ChatGPT Anweisungen**:

    Erweitere den Code von catalog, customer und order so das mittels /<service>/api/<id> nur der Eintrag laut <id> zurückgegeben wird

Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v2.0.1?ref_type=heads) ersichtlich.    

Version 2.0.2 (REST-Abfragen)
-------------

order holt Name Kunden und Produkt via REST-API von den anderen Services.

**ChatGPT Anweisungen**:

    Erweitere Order so, dass Customer und Product Name nicht fix im Code steht sondern via dem vorher erstellen API von Customer und Product geholt wird.

Das Resultat, mit ein paar Erweiterungen, ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v2.0.2?ref_type=heads) ersichtlich.      

**Tips und Tricks**  

Um die zu vielen Einträge in `order` zu eliminieren, einfach den Code an ChatGPT übergeben mit der Anweisung:

    Entferne customer_name und product_name von der Tabelle

Version 2.0.3 (REST POST order)
-------------

Erstellen einer Bestellung (order) mittels REST-API

**ChatGPT Anweisungen**:

    Erweitere den Code um mittels /order/api/new neue Orders via REST anfügen zu können.

Das Resultat ist [hier](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop/-/tree/v2.0.3?ref_type=heads) ersichtlich.

**Tips und Tricks**   

Der `curl` Aufruf um das API zu Testen kann auch von ChatGPT erzeugt werden.

    curl -X POST http://localhost:8080/order/order/api/new \
        -H "Content-Type: application/json" \
        -d '{
            "product_id": 5,
            "quantity": 3,
            "customer_id": 2
            }'

Das wir hinter einem Reverse Proxy sind, ist im URL `order/` einzufügen.   

Version 2.1.0 (Tabellarische Darstellung, Total rechnen)
-------------

Verbesserte tabellarische Darstellung, Total rechnen.

**ChatGPT Anweisungen**:

    Verbessere die Darstellung bei der HTML Ausgabe, dass alle Felder in Tabellenform angezeigt werden.
    
    <Code von catalog, customer, order kopieren>

Bei order Preis und Total nachführen

    Erweitere die Tabellenausgabe um den Preis und das Total quantity mal price

