Erstellen eines WebShop mittels ChatGPT
=======================================

[[_TOC_]]

Um die Funktionalität von [ChatGPT](https://chatgpt.com/) zu demonstrieren, erstellen wir einen WebShop für Autos und Auto Zubehör.

Eigenschaften
-------------

* Programmiersprache: Python
* Design Pattern: [Microservices](https://microservices.io)
* Bereitgestellt als vier Services catalog (Produktverwaltung), customer (Kundenverwaltung), order (Shop), webshop (Reverse Proxy und Menü)
* Aufbereitet als Container Images
* Version 1 - 3: Laufzeitumgebung - [Docker]() oder [PodMan]().
* Ab Version 4 : Laufzeitumgebung - Kubernetes.
   
Die Arbeitsschritte
* [Version 1 - erste Version](V1-Initial.md)
* [Version 2 - REST API, verbesserte Anzeige](V2-Shop.md)
* [Version 3 - Service Mesh](V3-ServiceMesh.md)
